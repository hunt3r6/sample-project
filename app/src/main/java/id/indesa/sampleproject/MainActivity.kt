package id.indesa.sampleproject

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import id.indesa.sampleproject.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(binding) {


            btnSend.setOnClickListener {
                val number = edtJumlah.text.toString()

                when {
                    number.isEmpty() -> {
                        Toast.makeText(
                            this@MainActivity,
                            "Silahkan Masukkan Jumlah penumpang",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    else -> {
                        val intent = Intent(this@MainActivity, ListPenumpangActivity::class.java)
                        intent.putExtra(ListPenumpangActivity.EXTRA_JUMLAH, number.toInt())
                        startActivity(intent)
                    }
                }


            }

        }
    }
}