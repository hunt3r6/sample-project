package id.indesa.sampleproject

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import id.indesa.sampleproject.databinding.ItemFormBinding

class ListAdapter(private val size: Int) : RecyclerView.Adapter<ListAdapter.ListViewHolder>() {
    class ListViewHolder(private val binding: ItemFormBinding) :
        RecyclerView.ViewHolder(binding.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        return ListViewHolder(
            ItemFormBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {

    }

    override fun getItemCount(): Int = size
}