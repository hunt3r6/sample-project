package id.indesa.sampleproject

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import id.indesa.sampleproject.databinding.ActivityListPenumpangBinding

class ListPenumpangActivity : AppCompatActivity() {
    private lateinit var listAdapter: ListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding = ActivityListPenumpangBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val data = intent.getIntExtra(EXTRA_JUMLAH, 0)
        listAdapter = ListAdapter(data)
        with(binding) {
            rvListPenumpang.apply {
                adapter = listAdapter
                setHasFixedSize(true)
                layoutManager = LinearLayoutManager(this@ListPenumpangActivity)
            }
        }

    }

    companion object {
        const val EXTRA_JUMLAH = "extra_jumlah"
    }
}